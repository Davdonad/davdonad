# davdonad


# davdonad

### Solucion Prueba Analitica

En este repositorio se encuentra la solucion a la prueba para el cargo Cientifico de Datos del CdE de Operación.

### Análisis Exploratorio de Obligaciones Financieras

Este es un proyecto de análisis exploratorio de datos proporcionados acerca de obligaciones financieras de ciertos clientes de una entidad Financiera. El objetivo de este análisis es comprender la relación existente entre calificaciones de riesgos, tasas de interes sobre las obligaciones, segmentos de negocios y asi poder generar una estimación acerca del saldo de la deuda de una obligacion.

### Fuentes de Datos

Este proyecto utiliza los siguientes conjuntos de datos:

- Obligaciones_clientes: Este conjunto de datos contiene información sobre las obligaciones de los clientes con un banco.
- tasas_productos: Este conjunto de datos contiene información sobre diferentes productos financieros y las tasas de interés correspondientes.

### Estructura del Repositorio

- SolucionPruebaTecnica_EduardoDonado.ipynb: el script de análisis exploratorio de datos, con las consultas SQL ejecutadas en Impala y la modelación final de los datos.

- Obligaciones_clientes.xlsx: archivo con las obligaciones de los clientes con un banco.

- tasas_productos.xlsx: archivo con los productos financieros y las tasas de interés correspondientes.

- requirements.txt: archivo con las librerias utilizadas en el desarrollo de la prueba.

- README.md: este archivo README.

### Librerías utilizadas
El análisis exploratorio de datos se realizó utilizando las siguientes librerías de Python:


- pandas
- numpy
- matplotlib
- seaborn
- plotly
- openpyxl
- scikit-learn
- statsmodels

Las conclusiones del análisis exploratorio de datos se encuentran en el archivo SolucionPruebaTecnica_EduardoDonado.ipynb.

### Autoría
Este proyecto fue creado por David Eduardo Donado Sierra.
